# Automatización DevOps 🔧

Scripts realizados en Bash para automatizar la instalación de Java, Docker, Kubernetes y Ansible en un sistema Linux

## Automatización Linux (Red Hat / Ubuntu)

Estos scripts actualizan la lista de paquetes y luego instalan Java, Docker, Kubernetes y Ansible según la disto correspondiente. 
Verifican la versión de cada componente después de la instalación para asegurarse de que se hayan instalado correctamente.

_Ejemplo de ejecución para Linux Red Hat:_

bash RedHatAutom.sh

## Otros scripts: AWS Cloudformation y Terraform

Archivo yaml para AWS Clowdformation con ejemplo para un bucket S3 y dos instancias EC2 con Ubuntu dentro de la capa gratuita de AWS, configurada para la región us-east-1a.

_Script para AWS CF:_

cf2.yaml

_Scripts Terraform:_

main.tf
variables.tf

En el caso de Terraform, el archivo main se podría separar en dos, uno para la EC2 y otro para la VPC. En este caso, todo está unificado en main a fines prácticos.

_Prerrequisitos:_
· Cuenta AWS
· AWS CLI instalado
· Archivo .pem para la EC2 (Ubuntu)
· Usuario IAM creado en la plataforma AWS (crear clave de acceso)
(_Nota: Un usuario de IAM es una identidad con credenciales válidas a largo plazo que se utiliza para interactuar con AWS en una cuenta._)
· Terraform instalado

_Comandos AWS:_
aws configure

_Comandos Terraform:_
terraform init
terraform plan
terraform apply