variable "region" {
  default = "us-east-1"
}

variable "vpc_cidr" {
  default = "10.0.0.0/22"
}

variable "public_subnets_cidr" {
  default = ["10.0.0.0/24", "10.0.1.0/24"]
}

variable "private_subnets_cidr" {
  default = ["10.0.2.0/24", "10.0.3.0/24"]
}
